import tensorflow as tf
import numpy as np
import keras
from keras import optimizers
from keras.datasets import mnist
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Activation, Dropout,Dense, Flatten, Convolution2D, MaxPooling2D

# load data and split into train and test data
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# reshape the inputs. add another dimension to process all RGB values
x_train = x_train.reshape(x_train.shape[0], 28, 28,1)
x_test = x_test.reshape(x_test.shape[0], 28, 28,1)

# Normalizing to get he float values
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')

# the train and test value should be upto 255 . since each byte represents 2^8 -1 values
x_train /= 255
x_test /= 255

'''
The Y values are the classes. 
From the MNIST problem defination we know that it will have 10 classes
'''

Y_train = keras.utils.np_utils.to_categorical(y_train, 10)
Y_test = np_utils.to_categorical(y_test, 10)

model = Sequential()

model.add(Convolution2D(32, (3, 3), activation='relu', input_shape=( 28, 28, 1)))
model.add(Convolution2D(32, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(10, activation='softmax'))

# Initializing the  optimizer. Classical algorithm Gradient Decent
sgd = optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)

# Compile the Model operation
model.compile(loss = "categorical_crossentropy", optimizer=sgd, metrics=['accuracy'])

# Fit the training inputs. On each batch 32 inputs and process will run 10 times to optimize the accuracy
model.fit(x_train, Y_train, batch_size=32, epochs=10)


score = model.evaluate(x_test, Y_test, verbose=0)

# saving the model

model.save('my_mnist.h5')

